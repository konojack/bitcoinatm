var proxy = require('express-http-proxy');
var express = require('express');
var app = express();
var server = app.listen(3000);
var io = require('socket.io').listen(server);
var childProcess = require('child_process');

var SerialPort = require('serialport');
//COM9 wcześniej
var port = new SerialPort('/dev/ttyACM0', function (err) {
    if (err) {
        return console.log('Error: ' + err.message);
    }
    baudRate: 9600

    console.log('seriel port open');


});

var ReadLine = SerialPort.parsers.Readline;
var parser = new ReadLine();
port.pipe(parser);

function onConnectionSocket(socket) {
    console.log("Socket connected");
}

io.on('connection', onConnectionSocket);

parser.on("data", function (data) {
    console.log(data);
    io.emit('BillAcceptor', { billAcceptor: data });
});


// put cash accepter in accept mode
port.write('FC05112756', 'hex', function (err) {
    if (err) {
        return console.log('Error on write: ', err.message);
    }
    console.log('Requested cash accepter to switch to accept mode');

});

app.use(express.static(__dirname));
app.get('/balance', function (req, res) {
    res.json(currentBalance, null, 200)
});
app.get('/printwallet', function (req, res) {
    const command = '/home/pi/bitcoinprinter/genandpri.sh';
    childProcess.exec(command, function (err, stdout, stderr) {
        if (err) {
            //some err occurred
            console.error(err)
        } else {
            // the *entire* stdout and stderr (buffered)
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        }
    })
})
app.get('/', function (req, res) {
    console.log('connection');
    res.sendfile(__dirname + '/index.html');
});

console.log('serving app on port 3000');
console.log(__dirname);