$(document).ready(function () {
    mainScreen();
});

/* @TODO: move this into config file, review server settings */
var currentBalance = 0;
var apiUrl = 'http://vps-6bfc3fc8.vps.ovh.net/api.php';
//////////
var completed;
var stopper;
var qrPublicKey;
var frozenBTCPrice;
var intervalPrintBTCPrice;
var declaredPLNPrice = 0;
var receivedBTCAmount = 0;
var finalBTCAmountToSell = 0;
var timerClock;

// CALCULATOR SIMPLE-KEYBOARD

let Keyboard = window.SimpleKeyboard.default;
let inputMask = window.SimpleKeyboardInputMask.default;

let myKeyboard = new Keyboard({
    onChange: input => onChange(input),
    onKeyPress: button => onKeyPress(button),
    modules: [inputMask],
    onModulesLoaded: () => console.log("MODULE LOADED!"),
    inputMask: {
        "default": {
            mask: '99999',
            regex: /^[0-9]+$/
        }
    },
    layout: {
        default: ["1 2 3", "4 5 6", "7 8 9", "RESET 0 {bksp}"]
    },
    theme: "hg-theme-default hg-layout-numeric numeric-theme",
    display: {
        'RESET': '❌',
        '{bksp}': '⌫'
    }
});


function onChange(input) {
    if (input == 'RESET') {
        myKeyboard.clearInput();
        document.querySelector(".input").value = 0;
    } else {
        document.querySelector(".input").value = input;
        $("#bookPrice .book-price-button").attr("disabled", false);
    }
    if (input && input != 'RESET') {
        calculateBTCPanel(parseInt(document.querySelector(".input").value));
    } else {
        myKeyboard.clearInput();
        document.querySelector(".input").value = 0;
        calculateBTCPanel(0);
        $("#bookPrice .book-price-button").attr("disabled", true);
    }

    declaredPLNPrice = document.querySelector(".input").value;
    // console.log("Input changed", input);
}


function onKeyPress(button) {
    if (button == 'RESET') {
        $("#bookPrice .book-price-button").attr("disabled", true);
        clearCalculator();
    }
}

function hideAll() {
    $("#getWalletBitcoin").hide();
    $("#loading").hide();
    $("#reallybuynow").hide();
    $("#bookPrice").hide();
    $("#summaryButtons").hide();
    $("#getQR").hide();
    $("#gotQR").hide();
    $("#completed").hide();
    $("#container").hide();
    $('#welcomeTextHeader').hide();
    $("#printWalletHeader").hide();
    $('.calculate-btc-wrapper').hide();
    $('.warningMessage').hide();
    $('#calculatorHeader').hide();
    $('#inputMoneyHeader').hide();
    $('#completedHeader').hide();
    $('#completedButton').hide();
    $('.wallet-choice-wrapper').hide();
    $('.summary-wrapper').hide();
    $('#actualPriceWrapper').hide();
    $('.regulations').hide();
}

function showCalculatorStep() {
    hideAll();
    $('#calculatorHeader').show();
    $('#actualPriceWrapper').show();
    $('.calculate-btc-wrapper').show();
    $('#bookPrice').show();
    $('.regulations').show();
    $('.warningMessage').show();
}

function showSummaryStep() {
    hideAll();
    connectMoneySocket();
    $('#inputMoneyHeader').show();
    $('.summary-wrapper').show();
    $("#summaryButtons").show();
    $('.regulations').show();
    $('.warningMessage').show();
}

function clearCalculator() {
    myKeyboard.clearInput();
    document.querySelector(".input").value = 0;
    calculateBTCPanel(0);
}

function mainScreen() {
    try {
        $('#reader').html5_qrcode_stop();
        $('#qr-canvas').remove();
        $('#video').remove();
    } catch (err) {
        console.log("Video Not On");
    }

    hideAll();
    clearCalculator();
    $('#actualPriceWrapper').show();
    $('#welcomeTextHeader').show();
    $('.wallet-choice-wrapper').show();
    $('.regulations').show();

    if (timerClock) {
        timerClock.reset();
        timerClock.stop();
    }

    // reset values
    currentBalance = 0;
    qrPublicKey = undefined;
    frozenBTCPrice = undefined;
    declaredPLNPrice = 0;
    receivedBTCAmount = 0;
    $("#bookPrice .book-price-button").attr("disabled", true);

    printBTCPrice();
    intervalPrintBTCPrice = setInterval(function () { printBTCPrice() }, 3000);

    $('.regulations').click(function () {
        swal({
            title: "REGULAMIN KORZYSTANIA Z BANKOMATU BTC",
            text: "1. Tutaj punkty z regulaminu",
            button: "OK"
        })
    })
}
/* @TODO: print Wallet script is missing
 *
 */
function printWallet(coin) {

    var data = {};
    var url = apiUrl;

    if (coin == "bitcoin") {

        data = "type=bitcoin&command=getnewaddress";
        url = apiUrl;

        $.ajax({
            type: "GET",
            url: url,
            data: data,
            headers: {
                "content-type": "text/plain"
            }
        });
    }

}

function bookPriceModal(coin) {
    clearInterval(intervalPrintBTCPrice);

    const mainBodyWrapper = document.createElement("div");

    const warningParagraph = document.createElement("p");
    warningParagraph.classList.add("warning");
    warningParagraph.textContent = "Prosimy o przemyślane zakupy, automat nie oddaje zdeponowanych banknotów, ani nie wydaje reszty."
    mainBodyWrapper.appendChild(warningParagraph);

    const btcRateWParagraph = document.createElement("p");
    btcRateWParagraph.classList.add("btc-rate");
    btcRateWParagraph.innerHTML = `Kurs wymiany <span>1 BTC = ${accounting.formatMoney(frozenBTCPrice, "")} PLN</span> będzie zarezerwowany jeszcze przez <span class="minutes"></span> minuty.`;
    mainBodyWrapper.appendChild(btcRateWParagraph);

    const declaredPLNParagraph = document.createElement("p");
    declaredPLNParagraph.classList.add("declared-value");
    declaredPLNParagraph.innerHTML = `Zadeklarowana kwota zakupu to <span>${declaredPLNPrice} PLN</span>.`;
    mainBodyWrapper.appendChild(declaredPLNParagraph);

    const receivedBTCParagraph = document.createElement("p");
    receivedBTCParagraph.classList.add("received-btc");
    receivedBTCParagraph.innerHTML = `Otrzymasz <span>${accounting.formatMoney(receivedBTCAmount, "", 8)} BTC</span>.`;
    mainBodyWrapper.appendChild(receivedBTCParagraph);

    const prepareBillsParagraph = document.createElement("p");
    prepareBillsParagraph.classList.add("received-btc", "warning");
    prepareBillsParagraph.textContent = `Proszę przygotować banknoty do wpłaty.`;
    mainBodyWrapper.appendChild(prepareBillsParagraph);

    // copy and create summary wrapper content
    const amount = document.querySelector(".summary-wrapper-amount");
    amount.innerHTML = '';

    amount.appendChild(btcRateWParagraph.cloneNode(true));
    amount.appendChild(declaredPLNParagraph.cloneNode(true));
    amount.appendChild(receivedBTCParagraph.cloneNode(true));

    const btcPublicAddress = document.querySelector(".btc-public-address");
    btcPublicAddress.innerHTML = `Adres portfela na jaki zostaną przelane środki: <span class="btc-wallet">${qrPublicKey}</span>`;


    timerClock = new easytimer.Timer();
    timerClock.start({ countdown: true, startValues: { minutes: 3 } });
    $('.btc-rate .minutes').html(timerClock.getTimeValues().toString());
    timerClock.addEventListener('secondsUpdated', function (e) {
        $('.btc-rate .minutes').html(timerClock.getTimeValues().toString());
    });
    timerClock.addEventListener('targetAchieved', function (e) {
        mainScreen();
        swal.close();
    });


    swal({
        className: "book-price-modal",
        title: "POTWIERDZENIE ZAKUPU",
        content: mainBodyWrapper,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "WRÓĆ",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "POTWIERDŹ",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then((data) => {
        if (data == null) {
            mainScreen();
            timerClock.reset();
            timerClock.stop();
        } else if (data == true) {
            showSummaryStep();
            // timerClock.reset();
        }
    })
}

function printBTCPrice() {
    if ($("#oneBTCPrice").is(":visible")) {
        $.get("https://blockchain.info/ticker", function (data) {
            let PLN = data.PLN.last;
            PLN = (PLN * .05);
            const amount_per = Math.round((data.PLN.last * .05) * 100 / 100) + data.PLN.last;
            frozenBTCPrice = amount_per;

            $("#actualPrice").text(accounting.formatMoney(amount_per, ""));

            if ($('input.money-value').val() != 0) {
                const $btcLabel = $('.amount-to-buy #BTC');
                let calculatorFooterValue = parseInt($('input.money-value').val()) / amount_per;
                receivedBTCAmount = calculatorFooterValue;
                $btcLabel.text(accounting.formatMoney(calculatorFooterValue, "", 8));
            }

        });
    }
}

function calculateBTCPanel(amount) {
    const $input = $(".money-value");
    const $plnLabel = $('.amount-to-buy #PLN');
    const $btcLabel = $('.amount-to-buy #BTC');
    $input.val(parseInt(amount));
    $plnLabel.text(amount);

    if (amount == 0) {
        $btcLabel.text(accounting.formatMoney(0, "", 8));
    } else {
        $.get("https://blockchain.info/ticker", function (data) {
            let PLN = data.PLN.last;
            PLN = (PLN * .05);
            const amount_per = Math.round((data.PLN.last * .05) * 100 / 100) + data.PLN.last;
            let total = parseInt(amount) / amount_per;
            total = total - 0.00001000;
            receivedBTCAmount = total;
            if (total < 0) {
                total = 0;
            }

            $btcLabel.text(accounting.formatMoney(total, "", 8));
        });
    }
}

function showCamera() {
    hideAll();
    $("#getQR").show();
    readNow();
}

function stopReading() {
    console.log("Stop");
    $('#reader').html5_qrcode_stop();
    clearInterval(stopper);
}


function readNow() {
    reading = 1;
    $('#reader').html5_qrcode(function (qrdata) {
        console.log("Starting Read: " + qrdata);
        let qr = qrdata;

        var res = qr.split(":");
        console.log("Res[0]: " + res[0] + " Res[1]:" + res[1] + " QR:" + qr);

        for (splited of res) {
            if (WAValidator.validate(splited)) {
                qrPublicKey = splited;
                break;
            }
        }

        if (qrPublicKey) {
            $("#getQR").hide();

            showCalculatorStep();
            stopper = setInterval(function () { stopReading() }, 100);
        }

    },
        function (error) {
            console.log("Err: " + error);
        },
        function (videoError) {
            console.log("vErr: " + videoError);
        });
}

function showCompleted() {
    hideAll();
    $("#completedHeader").show();
    $("#completed").show();
    $("#completedButton").show();

    $(".deposited-money").html(accounting.formatMoney(currentBalance, ""));
    $(".bought-btc").html(accounting.formatMoney(finalBTCAmountToSell, "", 8));
    $(".shipped-address").html(qrPublicKey);

    setTimeout(function () {
        mainScreen();
        location.reload();
    }, 10000);
}

function showPrintWallet(coin) {
    $.get("/printwallet", function () {
        console.log("PRINT WALLET STARTS");
    })
    hideAll();
    if (coin == 'bitcoin') {
        $("#printWalletHeader").show();
        $("#getWalletBitcoin").show();
        printWallet("bitcoin");
        redirectAfterShowWallet();
    }
    else {
        mainScreen();
    }

}

function redirectAfterShowWallet() {
    var counter = 10;
    var redirectMainScreen = setInterval(function () {
        $('#mainScreenRemaining').text(--counter);
        if (counter == 0) {
            clearInterval(redirectMainScreen);
            $("#getWalletBitcoin").hide();
            mainScreen();
            $('#mainScreenRemaining').text(10);
        }
    }, 1000);
}

function completePurchase() {
    var url = apiUrl;
    var amount = accounting.formatMoney(finalBTCAmountToSell, "", 8);
    var data = "type=bitcoin&command=send&addr=" + qrPublicKey + "&amount=" + amount;

    hideAll();
    timerClock.reset();
    timerClock.stop();
    $("#loading").show();

    $.ajax({
        type: "GET",
        url: url,
        data: data,
        headers: {
            "content-type": "text/plain"
        },
        success: completedSuccess
    });
}

function connectMoneySocket() {
    var socket = io.connect('http://localhost:3000');

    socket.on('BillAcceptor', function (data) {
        var billAcceptorData = data.billAcceptor;
        var properValue = billAcceptorData.substring(
            billAcceptorData.lastIndexOf("+") + 1,
            billAcceptorData.lastIndexOf("$")
        )

        properValue = parseInt(properValue);
        if (!isNaN(properValue)) {
            currentBalance += properValue;
            $(".buy-now-button").attr('disabled', false);

            $("#currentBalance").html(accounting.formatMoney(currentBalance, ""));

            const receivedBtc = currentBalance / frozenBTCPrice;
            $('.received-btc-final').text(accounting.formatMoney(receivedBtc, "", 8));

            finalBTCAmountToSell = receivedBtc;
        }
    })
}

function completedSuccess(res) {
    hideAll();
    showCompleted();
}